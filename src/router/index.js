import { progressBarMeta } from '@/helpers/progressbar-config'

import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Boost from '@/components/Boost'
import Booster from '@/components/Booster'
import Faq from '@/components/Faq'
import Offers from '@/components/Offers'

Vue.use(Router)

const router = new Router({
	mode: 'history',
  	routes: [
	    {
	      	path: '/',
	      	name: 'Home',
	      	component: Home,
	      	meta: {
	      		progress: progressBarMeta
	      	}
	    }, 
		{
			path: '/faq',
			name: 'faq',
			component: Faq,
			meta: {
	    		progress: progressBarMeta
	    	}
		},
	    {
	    	path: '/boost',
	    	name: 'Boost',
	    	component: Boost,
	    	meta: {
	    		progress: progressBarMeta
	    	}
	    },
	    {
	    	path: '/booster',
	    	name: 'booster',
	    	component: Booster,
	    	meta: {
	    		progress: progressBarMeta
	    	}
	    },
	    {
	    	path: '/offers',
	    	name: 'offers',
	    	component: Offers,
	    	meta: {
	    		progress: progressBarMeta
	    	}
	    }
  	]
})

export default router
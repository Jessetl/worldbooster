const progressBarOptions = {
  color: '#35c2a4',
  failedColor: '#cb161d',
  thickness: '3px',
  transition: {
	speed: '0.3s',
	opacity: '0.5s',
	termination: 300
  },
  autoRevert: true,
  location: 'top',
  inverse: false
}

const progressBarMeta = {
	func: [
		{call: 'color', modifier: 'temp', argument: '#35c2a4'},
		{call: 'fail', modifier: 'temp', argument: '#cb161d'},
		{call: 'location', modifier: 'temp', argument: 'top'},
		{call: 'transition', modifier: 'temp', argument: {speed: '1.5s', opacity: '0.6s', termination: 700}}
	]
}

export {
	progressBarOptions,
	progressBarMeta
}
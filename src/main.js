// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// 
import Vue from 'vue'
import App from './App'
// jQuery
import $ from 'jquery'
// Router
import router from './router'
// Progress Bar
import { progressBarOptions } from './helpers/progressbar-config'
import VueProgressBar from 'vue-progressbar'

Vue.config.productionTip = false

Vue.use(VueProgressBar, progressBarOptions)

import 'bootstrap'
import './assets/Worldbooster.scss'

let app

if (!app) {
	app = new Vue({
	  	el: '#app',
	  	router,
	  	render: h =>  h(App)
	})
}